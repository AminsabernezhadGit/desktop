'use strict';

const menu = new nw.Menu();

function Menu(cutLabel, copyLabel, pasteLabel) {
  const cut = new nw.MenuItem({
    label: cutLabel || 'Cut',
    click() {
      document.execCommand('cut');
    },
  });

  const copy = new nw.MenuItem({
    label: copyLabel || 'Copy',
    click() {
      document.execCommand('copy');
    },
  });

  const paste = new nw.MenuItem({
    label: pasteLabel || 'Paste',
    click() {
      document.execCommand('paste');
    },
  });
  menu.append(cut);
  menu.append(copy);
  menu.append(paste);

  return menu;
}

module.exports = new Menu();
